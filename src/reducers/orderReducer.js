export const order = (state = [], action) => {

    switch (action.type) {
        case 'POST_ORDER':
            return action.payload;
        case 'CLEAR_ALL':
            return [];
        default:
            return state;
    }    
};