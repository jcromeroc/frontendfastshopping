export const summary = (state = [], action) => {

    switch (action.type) {
        case 'SAVE_SUMMARY':
            return action.payload;
        case 'POST_SUMMARY':
            return action.payload;
        case 'CLEAR_ALL':
            return [];
        default:
            return state;
    }    
};