export const cart = (state = [], action) => {

    switch (action.type) {
        case 'ADD_TO_CART':
            if(state.indexOf(action.payload) === -1) {
                return [ ...state, action.payload];
            }
            return state;
        case 'DELETE_PRODUCT_CART':
            let productsCart = [...state];
            if(productsCart.indexOf(action.payload) !== -1) {
                productsCart.splice(productsCart.indexOf(action.payload), 1);
                return productsCart;
            }
            return state;
        case 'CLEAR_ALL':
            return [];
        default:
            return state;
    }    
};