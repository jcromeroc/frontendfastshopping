export const page = (state = 1, action) => {

    switch (action.type) {
        case 'SAVE_PAGE':
            return action.payload;
        case 'CLEAR_ALL':
            return 1;
        default:
            return state;
    }    
};