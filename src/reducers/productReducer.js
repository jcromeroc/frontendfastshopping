export const products = (state = [], action) => {

    switch (action.type) {
        case 'FETCH_PRODUCTS':
            return action.payload;
        case 'FETCH_PRODUCTS_PAGE_SORT':
            return action.payload;
        case 'CLEAR_ALL':
            return [];
        default:
            return state;
    }    
};