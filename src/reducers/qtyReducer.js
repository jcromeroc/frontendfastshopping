export const qty = (state = [], action) => {

    switch (action.type) {
        case 'ADD_QTY':
            return action.payload;
        case 'CLEAR_ALL':
            return [];
        default:
            return state;
    }    
};