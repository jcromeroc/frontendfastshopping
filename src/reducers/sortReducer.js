export const sort = (state = "", action) => {

    switch (action.type) {
        case 'SAVE_SORT':
            return action.payload;
        case 'CLEAR_ALL':
            return "";
        default:
            return state;
    }    
};