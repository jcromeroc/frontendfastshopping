import { combineReducers } from 'redux';
import { products } from './productReducer';
import { page } from './pageReducer';
import { sort } from './sortReducer';
import { cart } from './cartReducer';
import { qty } from './qtyReducer';
import { customer } from './customerReducer';
import { order } from './orderReducer';
import { summary } from './summaryReducer';

export default combineReducers({
    products,
    page,
    sort,
    cart,
    qty,
    customer,
    summary,
    order
});