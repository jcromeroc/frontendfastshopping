export const customer = (state = [], action) => {

    switch (action.type) {
        case 'GET_USER_BY_EMAIL':
            return action.payload;
        case 'POST_CUSTOMER':
            return action.payload;
        case 'CLEAR_ALL':
            return [];
        default:
            return state;
    }    
};