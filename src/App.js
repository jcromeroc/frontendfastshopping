import React from 'react';
import './App.css';

import { BrowserRouter as Router, Route, Redirect, Switch } from "react-router-dom";
import Header from './components/Header';
import Main from './components/Main';
import ShoppingCart from './components/ShoppingCart';
import PlaceOrder from './components/PlaceOrder';
import Thanks from './components/Thanks';

function App() {
  return (
      <Router>
        <header>
          <Header/>
        </header>
        <main>
          <Switch>
            <Route path="/" exact>
              <Main />
            </Route>
            <Route path="/cart" exact>
              <ShoppingCart />
            </Route>
            <Route path="/checkout" exact>
              <PlaceOrder />
            </Route>
            <Route path="/thanks" exact>
              <Thanks />
            </Route>
            <Redirect to="/" />
          </Switch>
        </main>
      </Router>
  );
}

export default App;
