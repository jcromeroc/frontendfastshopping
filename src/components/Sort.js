import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { InputLabel, MenuItem, FormControl, Select } from '@material-ui/core';
import { connect } from 'react-redux';
import { fetchProductsByPageAndOrder, saveSort } from '../actions';

const useStyles = (theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
});

class Sort extends Component {

  constructor(props) {
    super(props);
    this.state = { 
        sort: this.props.sort,
        page: this.props.page,
        open: false
    };
  }

  handleChange = (event) => {
    this.setState({
      sort: event.target.value
    }, () => {
      this.props.saveSort(this.state.sort);
    });
  }

  handleClose = () => {
    this.setState({
      open: false
    });
  }

  handleOpen = () => {
    this.setState({
      open: true
    });
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    
    if (this.props.page !== prevProps.page) {
      this.setState({
        page: this.props.page
      });
    }
    
    if (this.state.sort !== prevState.sort) {
        this.props.fetchProductsByPageAndOrder(this.state.page, this.state.sort);
    }
  }

  render() {
    const { classes } = this.props;
    return (
      <FormControl className={classes.formControl}>
          <InputLabel id="sort">Sort</InputLabel>
          <Select
              labelId="sort"
              id="sort-select"
              open={this.state.open}
              onClose={this.handleClose}
              onOpen={this.handleOpen}
              value={this.state.sort}
              onChange={this.handleChange}
          >
          <MenuItem value="">
              <em>Default</em>
          </MenuItem>
          <MenuItem value={"AlphaOrder"}>Alpha Order</MenuItem>
          <MenuItem value={"LowestPrice"}>Lowest Price</MenuItem>
          <MenuItem value={"MostRecent"}>Most Recent</MenuItem>
        </Select>
      </FormControl>
    );
  }
};

const mapStateToProps = (state) => {
  return { 
    page: state.page,
    sort: state.sort
  };
}

export default connect(mapStateToProps,{ fetchProductsByPageAndOrder, saveSort })(withStyles(useStyles, { withTheme: true })(Sort));