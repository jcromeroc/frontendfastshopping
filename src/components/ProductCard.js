import React, { Component, Fragment } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { Grid, Chip, Card, CardContent, CardMedia, Typography, Button} from '@material-ui/core';

import { connect } from 'react-redux';
import { fetchProducts, addToCart } from '../actions';

const useStyles = (theme) => ({
    root: {
        display: 'flex',
        height: "100%",
    },
    details: {
        display: 'flex',
        flexDirection: 'column',
    },
    content: {
        flex: '1 0 auto',
    },
    cover: {
        width: '100%',
    },
    chip: {
        display: 'flex',
        flexWrap: 'wrap',
        listStyle: 'none',
        margin: 0,
        padding: 0,
    },
});
  
class CardProduct extends Component {

    componentDidMount() {
        this.props.fetchProducts();
    }

    renderCategorys = (categorys) => {
        let arrayCategorys = categorys.split(',');
        let arrayBadge = arrayCategorys.map((category, index) => {
            return (
                <li key={index} ml={5}>
                    <Chip label={category} />
                </li>
            );
        });
        return arrayBadge;
    }

    render() {
        const { classes, products } = this.props;
        return (
            <Fragment>
                { ((products.code === "200") && products.data && products.data.length > 0) ? products.data.map((product) => {
                    return(
                        <Grid item xs={12} md={6} key={product.Id}>
                            <Card className={classes.root}>
                                <CardMedia
                                    className={classes.cover}
                                    image={`/images/${product.Image}`}
                                    title={product.Title}
                                />
                                <div className={classes.details}>
                                    <CardContent className={classes.content}>
                                        <Grid container direction="row" justify="flex-start" alignItems="flex-start" spacing={1}>
                                            <Grid item xs={12}>
                                                <Typography component="h5" variant="h5">
                                                    {product.Title}
                                                </Typography>
                                            </Grid>
                                            <Grid item xs={12} component="ul" className={classes.chip}>
                                                {this.renderCategorys(product.Category)}
                                            </Grid>
                                            <Grid item xs={12}>
                                                <Typography variant="body1" gutterBottom>
                                                    {product.Description}
                                                </Typography>
                                            </Grid>
                                            <Grid item xs={7}>
                                                <Button variant="contained" color="primary" onClick={() => { this.props.addToCart(product) }}>
                                                    Add to cart
                                                </Button>
                                            </Grid>
                                            <Grid item xs={5}>
                                                <Typography component="h5" variant="h5">
                                                    $ {product.Price}
                                                </Typography>
                                            </Grid>
                                        </Grid>
                                    </CardContent>
                                </div>
                            </Card>
                        </Grid>
                    );
                }) : 
                    <Grid container direction="row" justify="center" alignItems="center" spacing={1}>
                        <Typography component="h1" variant="h1">
                            Not Products
                        </Typography>
                    </Grid>
                }
            </Fragment>
        );
    }
}

const mapStateToProps = (state) => {
    return { products: state.products };
}

export default connect(mapStateToProps,{ fetchProducts, addToCart })(withStyles(useStyles, { withTheme: true })(CardProduct));
