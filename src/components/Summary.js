import React, { Component, Fragment } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { Grid, Box, Typography, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper } from '@material-ui/core';
import { connect } from 'react-redux';
import { saveSummary } from '../actions';


const useStyles = (theme) => ({
    table: {
        minWidth: 700,
    },
});

const StyledTableCell = withStyles((theme) => ({
    head: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white,
    },
    body: {
      fontSize: 14,
    },
  }))(TableCell);
  
const StyledTableRow = withStyles((theme) => ({
    root: {
      '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.action.hover,
      },
    },
}))(TableRow);
  
class Summary extends Component {

    constructor(props) {
        super(props);
        this.state = {
            summary: null,
            newOrder: false,
            type: "",
            customer: null 
        };
    }

    createData = (product, price, qty, total) => {
        return { product, price, qty, total };
    }

    createDataSummary = (id, product, price, qty, total) => {
        return { id, product, price, qty, total };
    }

    createRow = () => {
        let data = this.props.cart.map((item) => {
            return this.createData(item.Title, item.Price, this.props.qty[`qty${item.Id}`], parseFloat(item.Price) * this.props.qty[`qty${item.Id}`]);
        });

        return data;
    }

    createRowSumary = () => {
        let data = this.props.cart.map((item) => {
            return this.createDataSummary(item.Id ,item.Title, item.Price, this.props.qty[`qty${item.Id}`], parseFloat(item.Price) * this.props.qty[`qty${item.Id}`]);
        });

        return data;
    }

    totalOrder = (row) => {
        let total = 0;

        row.map((item) => {
            total = total + item.total;
            return true;
        })

        return total;
    }

    componentDidMount() {
        let rows = this.createRowSumary();
        let total = this.totalOrder(rows);

        this.props.saveSummary({rows, total});
    }

    render() {
        const { classes } = this.props;
        const rows = this.createRow();
        const total = this.totalOrder(rows);
        return (
            <Fragment>
                <Box mt={5}>
                    <Typography component="h4" variant="h4">
                        Order Summary
                    </Typography>
                </Box>
                <TableContainer component={Paper} mt={2}>
                    <Table className={classes.table} aria-label="customized table">
                        <TableHead>
                        <TableRow>
                            <StyledTableCell>Product</StyledTableCell>
                            <StyledTableCell align="right">Unit Price</StyledTableCell>
                            <StyledTableCell align="right">Units</StyledTableCell>
                            <StyledTableCell align="right">Total Price</StyledTableCell>
                        </TableRow>
                        </TableHead>
                        <TableBody>
                        {rows.map((row) => (
                            <StyledTableRow key={row.product}>
                                <StyledTableCell component="th" scope="row">
                                    {row.product}
                                </StyledTableCell>
                                <StyledTableCell align="right">{row.price}</StyledTableCell>
                                <StyledTableCell align="right">{row.qty}</StyledTableCell>
                                <StyledTableCell align="right">{row.total}</StyledTableCell>
                            </StyledTableRow>
                        ))}
                        </TableBody>
                    </Table>
                </TableContainer>
                <Grid container direction="row" justify="flex-end" alignItems="flex-end" spacing={1}>
                    <Grid item xs={3}>
                        <Box display="flex" flexDirection="row" mb={1}>
                            <Typography omponent="h5" variant="h5">
                                Total: $ {total}
                            </Typography>
                        </Box>
                    </Grid>
                </Grid>
            </Fragment>
        );
    }
}

const mapStateToProps = (state) => {
    return { 
        cart: state.cart,
        qty: state.qty
    };
}

export default connect(mapStateToProps, { saveSummary })(withStyles(useStyles, { withTheme: true })(Summary));
