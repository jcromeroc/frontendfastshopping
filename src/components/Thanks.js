import React, { Component, Fragment } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { Grid, Box, Typography, Button } from '@material-ui/core';
import { connect } from 'react-redux';
import { clearAll } from '../actions';
import SentimentVerySatisfiedIcon from '@material-ui/icons/SentimentVerySatisfied';
import { Link } from 'react-router-dom';


const useStyles = (theme) => ({
    root: {
      flexGrow: 1,
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    title: {
      flexGrow: 1,
    },
    offset: theme.mixins.toolbar,
    colorLink: {
        color: "white"
    }
});
  
class Thanks extends Component {

    constructor(props) {
        super(props);
        this.state = {
            orderId: null,
            userName: null,
        };
    }

    componentDidMount() {
        this.setState({
            orderId: this.props.order.id,
            userName: Object.keys(this.props.customer).length > 0 ? this.props.customer.data.Full_name : null
        });
        this.props.clearAll();
    }

    render() {
        //const { classes } = this.props;

        return (
            <Fragment>
                <Grid container direction="row" justify="center" alignItems="center" spacing={1}>
                    <Grid item xs={1}>
                        <Box mt={5}>
                            <SentimentVerySatisfiedIcon style={{ fontSize: 100 }} />
                        </Box>
                    </Grid>
                </Grid>
                <Grid container direction="row" justify="center" alignItems="center" spacing={1}>
                    <Grid item xs={7}>
                        <Box mt={2} ml={7}>
                            <Typography variant="h2" >
                                Thanks for your purchase
                            </Typography>
                        </Box>
                    </Grid>
                </Grid>
                <Grid container direction="row" justify="center" alignItems="center" spacing={1}>
                    <Grid item xs={6}>
                        <Box mt={2} ml={7}>
                            <Typography variant="body1" >
                                {this.state.userName}, we have created you order #{this.state.orderId}. Your items will be soon at your door
                            </Typography>
                        </Box>
                    </Grid>
                </Grid>
                <Grid container direction="row" justify="center" alignItems="center" spacing={1}>
                    <Grid item xs={2}>
                        <Box mt={2} ml={7}>
                            <Typography variant="body1" >
                                Stay safe.
                            </Typography>
                        </Box>
                    </Grid>
                </Grid>
                <Grid container direction="row" justify="center" alignItems="center" spacing={1}>
                    <Grid item xs={3}>
                        <Box mt={2} ml={10}>
                            <Link to="/">
                                <Button variant="contained" color="primary">
                                    Start Again
                                </Button>
                            </Link>
                        </Box>
                    </Grid>
                </Grid>
            </Fragment>
        );
    }
}

const mapStateToProps = (state) => {
    return { 
        order: state.order,
        customer: state.customer
    };
}

export default connect(mapStateToProps, { clearAll })(withStyles(useStyles, { withTheme: true })(Thanks));
