import React, { Component, Fragment} from 'react';
import { withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import { postCustomer, postOrder, postOrderSummary } from '../actions';
import { Grid, Button } from '@material-ui/core';
import { Redirect } from 'react-router-dom';

import Customer from './Customer';
import Summary from './Summary';

const useStyles = (theme) => ({
    root: {
        display: 'flex',
        height: "100%",
    },
    details: {
        display: 'flex',
        flexDirection: 'column',
        width: '100%'
    },
    content: {
        flex: '1 0 auto',
    },
    cover: {
        width: '10%',
    },
    chip: {
        display: 'flex',
        flexWrap: 'wrap',
        listStyle: 'none',
        margin: 0,
        padding: 0,
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },
});

class PlaceOrder extends Component {

    constructor(props) {
        super(props);
        this.state = {
            redirect: null,
            newOrder: false,
            type: "",
            customer: null 
        };
    }

    placeOrder = async () => {
        if (this.state.type === "New") {
            await this.props.postCustomer(this.state.customer);
            await this.props.postOrder({
                Date: new Date(),
                Total: this.props.summary.total,
                Customer_Id: this.state.customer.Id
            });
            this.props.summary.rows.map( async (item) => {
                await this.props.postOrderSummary({
                    Units: item.qty,
                    Total_price: item.total,
                    Order_Id: this.props.order.id,
                    Product_Id: item.id
                });
                return true;
            });
        }

        if (this.state.type === "ExistingCustomer") {
            await this.props.postOrder({
                Date: new Date(),
                Total: this.props.summary.total,
                Customer_Id: this.props.customer.data.Id
            });
            this.props.summary.rows.map( async (item) => {
                await this.props.postOrderSummary({
                    Units: item.qty,
                    Total_price: item.total,
                    Order_Id: this.props.order.id,
                    Product_Id: item.id
                });
                return true;
            });
        }

        setTimeout(() => {
            this.setState({ redirect: "/thanks" });
        }, 500);
    }

    newOrder = (flag, type, customer = null) => {
        this.setState({
            newOrder: flag,
            type,
            customer
        });
    }

    render() {

        if (this.state.redirect) {
            return <Redirect to={this.state.redirect} />
        }

        return (
            <Fragment>
                <Grid container direction="row" justify="flex-start" alignItems="flex-start" spacing={1}>
                    <Grid item xs={5}>
                        <Customer new={this.newOrder}/>
                    </Grid>
                    <Grid item xs={7}>
                        <Summary />
                    </Grid>
                </Grid>
                { this.state.newOrder ?
                    <Grid container direction="row" justify="flex-end" alignItems="flex-end" spacing={1}>
                        <Grid item xs={2}>
                            <Button variant="contained" color="primary" onClick={() => { this.placeOrder() }}>
                                Place Order
                            </Button>
                        </Grid>
                    </Grid>
                    :
                    null
                }
            </Fragment>
        );
    }
};

const mapStateToProps = (state) => {
    return { 
        summary: state.summary,
        customer: state.customer,
        order: state.order
    };
}

export default connect(mapStateToProps, { postCustomer, postOrder, postOrderSummary })(withStyles(useStyles, { withTheme: true })(PlaceOrder));