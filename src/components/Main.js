import React, { Component, Fragment } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Sort from './Sort';
import ProductCard from './ProductCard';
import { Grid, Box } from '@material-ui/core';
import Pagination from '@material-ui/lab/Pagination';
import { connect } from 'react-redux';
import { fetchProductsByPageAndOrder, savePage } from '../actions';

const useStyles = (theme) => ({
    root: {
        '& > *': {
          marginTop: theme.spacing(2),
        },
    },
});

class Main extends Component {

    constructor(props) {
        super(props);
        this.state = { 
            page: this.props.page,
            sort: this.props.sort,
        };
    }

    handleChange = (event, value) => {
        this.setState({
            page: value
        }, () => {
            this.props.savePage(this.state.page);
        });
    }

    componentDidUpdate(prevProps, prevState, snapshot) {

        if (this.props.sort !== prevProps.sort) {
            this.setState({
                sort: this.props.sort
            });
        }

        if (this.state.page !== prevState.page) {
            this.props.fetchProductsByPageAndOrder(this.state.page, this.state.sort);
        }
    }


    render() {
        const { classes, products } = this.props;
        return (
            <Fragment>
                <Grid container spacing={5}>
                    <Grid item xs={12}>
                    <Box display="flex" flexDirection="row-reverse" m={0}>
                        <Sort />
                    </Box>
                    </Grid>
                </Grid>
                <Grid container spacing={2}>
                    <ProductCard/>
                </Grid>
                <Grid container direction="row" justify="center" alignItems="center" spacing={2}>
                    <div className={classes.root}>
                        <Pagination count={products.numPages} page={this.state.page} onChange={this.handleChange} color="primary" mb={4}/>
                    </div>
                </Grid>
            </Fragment>
        );
    }
};

const mapStateToProps = (state) => {
    return { 
        products: state.products,
        page: state.page,
        sort: state.sort,
    };
}

export default connect(mapStateToProps,{ fetchProductsByPageAndOrder, savePage })(withStyles(useStyles, { withTheme: true })(Main));
