import React, { Component, Fragment } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { Paper, Box, Grid, Typography, Radio, TextField, Button, Link } from '@material-ui/core';
import { connect } from 'react-redux';
import { getUserByEmail } from '../actions';

const useStyles = (theme) => ({
    root: {
      flexGrow: 1,
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    title: {
      flexGrow: 1,
    },
    offset: theme.mixins.toolbar,
    colorLink: {
        color: "white"
    },
    radiosText: {
        display: 'inline-block',
    }
});
  
class Customer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            are: "New",
            name: "",
            id: "",
            address: "",
            phone: "",
            email: "",
            customer: null,
            lookup: false,
        };
    }

    handleChangeRadio = (event) => {
        this.setState({
            are: event.target.value,
        });
        this.props.new(false, this.state.are);
    };

    handleChangeInput = (event) => {
        this.setState({
            [event.target.name]: event.target.value,
        });
    };

    searchUser = async () => {
        this.setState({
            lookup: true,
            are: "ExistingCustomer"
        });

        if (this.state.email !== "") {
            await this.props.getUserByEmail(this.state.email);
            if( this.props.customer.code === "200") {
                this.setState({
                    customer: this.props.customer.data
                });
                this.props.new(true, this.state.are);
            }
        }
    }

    lookupAgain = () => {
        this.setState({
            lookup: false,
            are: "Existing",
            email: "",
            customer: null
        });
        this.props.new(false, this.state.are);
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.state !== prevState) {
            if (this.state.name !== '' && 
                this.state.id !== '' &&
                this.state.address !== '' &&
                this.state.phone !== '' &&
                this.state.email !== '') {
                this.props.new(true, this.state.are, {
                    Id: this.state.id,
                    Full_name: this.state.name,
                    Address: this.state.address,
                    Phone_number: this.state.phone,
                    Email: this.state.email
                });
            } else {
                this.props.new(false, this.state.are);
            }
        }
    }

    render() {
        const { classes } = this.props;
        return (
            <Fragment>
                <Box mt={5}>
                    <Typography component="h4" variant="h4">
                        Customer Information
                    </Typography>
                </Box>
                <Box mt={2}>
                    <Paper elevation={3} mt={2}>
                        <Grid container direction="row" justify="flex-start" alignItems="flex-start" spacing={1}>
                            <Grid item xs={12}>
                                <Grid container direction="row" justify="flex-start" alignItems="flex-start" spacing={1}>
                                    <Grid item xs={4}>
                                        <Box mt={1} ml={2}>
                                            <Typography variant="body1" gutterBottom>
                                                Are you?
                                            </Typography>
                                        </Box>
                                    </Grid>
                                    <Grid item xs={8}>
                                        <div>
                                            <Radio
                                                checked={this.state.are === 'New'}
                                                onChange={this.handleChangeRadio}
                                                value="New"
                                                name="youAre"
                                                inputProps={{ 'aria-label': 'New' }}
                                            />
                                            <Typography variant="body1" gutterBottom className={classes.radiosText}>
                                                New Customer
                                            </Typography>
                                            <Radio
                                                checked={this.state.are === 'Existing' || this.state.are === 'ExistingCustomer'}
                                                onChange={this.handleChangeRadio}
                                                value="Existing"
                                                name="youAre"
                                                inputProps={{ 'aria-label': 'Existing' }}
                                            />
                                            <Typography variant="body1" gutterBottom className={classes.radiosText}>
                                                Existing Customer
                                            </Typography>
                                        </div>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                        { this.state.are === "New" ? 
                            <Grid item xs={12}>
                                <Grid container direction="row" justify="flex-start" alignItems="flex-start" spacing={1}>
                                    <Grid item xs={4}>
                                        <Box mt={2} ml={2}>
                                            <Typography variant="body1" gutterBottom>
                                                Full Name
                                            </Typography>
                                        </Box>
                                    </Grid>
                                    <Grid item xs={8}>
                                        <TextField id="full-name" label="Full Name" name="name" onChange={(event) => {this.handleChangeInput(event)}}/>
                                    </Grid>
                                    <Grid item xs={4}>
                                        <Box mt={2} ml={2}>
                                            <Typography variant="body1" gutterBottom>
                                                ID
                                            </Typography>
                                        </Box>
                                    </Grid>
                                    <Grid item xs={8}>
                                        <TextField id="id" label="ID" name="id" type="number" onChange={(event) => {this.handleChangeInput(event)}}/>
                                    </Grid>
                                    <Grid item xs={4}>
                                        <Box mt={2} ml={2}>
                                            <Typography variant="body1" gutterBottom>
                                                Address
                                            </Typography>
                                        </Box>
                                    </Grid>
                                    <Grid item xs={8}>
                                        <TextField id="address" label="Address" name="address" onChange={(event) => {this.handleChangeInput(event)}} multiline rows={2}/>
                                    </Grid>
                                    <Grid item xs={4}>
                                        <Box mt={2} ml={2}>
                                            <Typography variant="body1" gutterBottom>
                                                Phone Number
                                            </Typography>
                                        </Box>
                                    </Grid>
                                    <Grid item xs={8}>
                                        <TextField id="phone" label="Phone Number" name="phone" onChange={(event) => {this.handleChangeInput(event)}}/>
                                    </Grid>
                                    <Grid item xs={4}>
                                        <Box mt={2} ml={2}>
                                            <Typography variant="body1" gutterBottom>
                                                Email
                                            </Typography>
                                        </Box>
                                    </Grid>
                                    <Grid item xs={8}>
                                        <TextField id="email" label="Email" name="email" onChange={(event) => {this.handleChangeInput(event)}}/>
                                    </Grid>
                                </Grid>
                            </Grid>
                            :
                            null
                        }
                        { this.state.are === "Existing" ? 
                            <Grid item xs={12}>
                                <Grid container direction="row" justify="flex-start" alignItems="flex-start" spacing={1}>
                                    <Grid item xs={4}>
                                        <Box mt={2} ml={2}>
                                            <Typography variant="body1" gutterBottom>
                                                Email
                                            </Typography>
                                        </Box>
                                    </Grid>
                                    <Grid item xs={8}>
                                        <TextField id="email" label="Email" name="email" onChange={(event) => {this.handleChangeInput(event)}}/>
                                    </Grid>
                                </Grid>
                                <Grid container direction="row" justify="flex-end" alignItems="flex-end" spacing={1}>
                                    <Grid item xs={4}>
                                        <Box mt={2} ml={2}>
                                        <Button variant="contained" color="primary" onClick={() => { this.searchUser() }}>
                                            Lookup
                                        </Button>
                                        </Box>
                                    </Grid>
                                </Grid>
                            </Grid>
                            :
                            null
                        }
                        {
                            (this.state.are === "ExistingCustomer" && this.state.lookup && this.state.customer === null) ?
                                <Fragment>
                                    <Grid container direction="row" justify="center" alignItems="center" spacing={1}>
                                        <Grid item xs={6}>
                                            <Box mt={2}>
                                                <Typography component="h6" variant="h6">
                                                    Customer not found
                                                </Typography>
                                            </Box>
                                        </Grid>
                                    </Grid>
                                    <Grid container direction="row" justify="flex-start" alignItems="flex-start" spacing={1}>
                                        <Grid item xs={12}>
                                            <Box mt={1}>
                                                <Link href="#" onClick={() => {this.lookupAgain()}}>
                                                    Lookup again
                                                </Link>
                                            </Box>
                                        </Grid>
                                    </Grid>
                                </Fragment>
                                :
                                null
                        }
                        {
                            (this.state.are === "ExistingCustomer" && this.state.lookup && this.state.customer !== null) ?
                                <Fragment>
                                    <Grid container direction="row" justify="center" alignItems="center" spacing={1}>
                                        <Grid item xs={12}>
                                            <Box m={2}>
                                                <Typography component="h4" variant="h4">
                                                    Welcome back, {this.state.customer.Full_name}
                                                </Typography>
                                            </Box>
                                        </Grid>
                                        <Grid item xs={12}>
                                            <Box ml={2}>
                                                <Typography variant="body1" gutterBottom>
                                                    ID: {this.state.customer.Id}
                                                </Typography>
                                            </Box>
                                        </Grid>
                                        <Grid item xs={12}>
                                            <Box ml={2}>
                                                <Typography variant="body1" gutterBottom>
                                                    Address: {this.state.customer.Address}
                                                </Typography>
                                            </Box>
                                        </Grid>
                                        <Grid item xs={12}>
                                            <Box ml={2}>
                                                <Typography variant="body1" gutterBottom>
                                                    Phone Number: {this.state.customer.Phone_number}
                                                </Typography>
                                            </Box>
                                        </Grid>
                                        <Grid item xs={12}>
                                            <Box ml={2}>
                                                <Typography variant="body1" gutterBottom>
                                                    Email: {this.state.customer.Email}
                                                </Typography>
                                            </Box>
                                        </Grid>
                                    </Grid>
                                    <Grid container direction="row" justify="flex-start" alignItems="flex-start" spacing={1}>
                                        <Grid item xs={12}>
                                            <Box mt={1}>
                                                <Link href="#" onClick={() => {this.lookupAgain()}}>
                                                    Not {this.state.customer.Full_name}?, Lookup again
                                                </Link>
                                            </Box>
                                        </Grid>
                                    </Grid>
                                </Fragment>
                                :
                                null
                        }
                    </Paper>
                </Box>
            </Fragment>
        );
    }
}

const mapStateToProps = (state) => {
    return { 
        cart: state.cart,
        customer: state.customer,
    };
}

export default connect(mapStateToProps, { getUserByEmail })(withStyles(useStyles, { withTheme: true })(Customer));
