import React, { Component, Fragment} from 'react';
import { withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import { deleteProductCard, addQtys } from '../actions';
import { Grid, Box, Chip, Card, CardContent, CardMedia, Typography, Button, InputLabel, MenuItem, FormControl, Select} from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import { Link, Redirect } from 'react-router-dom';

const useStyles = (theme) => ({
    root: {
        display: 'flex',
        height: "100%",
    },
    details: {
        display: 'flex',
        flexDirection: 'column',
        width: '100%'
    },
    content: {
        flex: '1 0 auto',
    },
    cover: {
        width: '10%',
    },
    chip: {
        display: 'flex',
        flexWrap: 'wrap',
        listStyle: 'none',
        margin: 0,
        padding: 0,
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },
});

class ShoppingCart extends Component {

    constructor(props) {
        super(props);
        this.state = this.initialState();
    }

    initialState = () => {
        let initial = {redirect: null, total : 0};
        this.props.cart.map((item) => {
            initial = {...initial, [`qty${item.Id}`]: 1, [`open${item.Id}`]: false };
            return true;
        });

        return initial;
    }

    renderCategorys = (categorys) => {
        let arrayCategorys = categorys.split(',');
        let arrayBadge = arrayCategorys.map((category, index) => {
            return (
                <li key={index} ml={5}>
                    <Chip label={category} />
                </li>
            );
        });
        return arrayBadge;
    }

    handleChange = (event) => {
        this.setState({
            [`qty${event.target.name}`]: parseInt(event.target.value),
            [`open${event.target.name}`]: false
        });
    }
    
    handleClose = (event) => {
        this.setState({
            [`open${event.target.id}`]: false
        });
    }
    
    handleOpen = (event) => {
        this.setState({
            [`open${event.target.id}`]: true
        });
    }

    sumTotal = () => {
        let total = 0;

        this.props.cart.map((item) => {
            total = total + this.state[`qty${item.Id}`] * parseFloat(item.Price);
            return true;
        })

        return total;
    }

    checkout = () => {
        this.props.addQtys(this.state);
        this.setState({ redirect: "/checkout" });
    }

    render() {
        const { classes, cart } = this.props;

        if (this.state.redirect) {
            return <Redirect to={this.state.redirect} />
        }

        return (
            <Fragment>
                <Grid container direction="row" justify="flex-start" alignItems="flex-start" spacing={1}>
                    <Grid item xs>
                        <Box display="flex" flexDirection="row" mb={5}>
                            <Typography component="h3" variant="h3">
                                Shopping Cart
                            </Typography>
                        </Box>
                    </Grid>
                </Grid>
                { (cart && cart.length > 0) ?
                    <Grid container spacing={3}>
                        { cart.map((product) => {
                            return(
                                <Grid item xs={12} key={product.Id}>
                                    <Card className={classes.root}>
                                        <CardMedia
                                            className={classes.cover}
                                            image={`/images/${product.Image}`}
                                            title={product.Title}
                                        />
                                        <div className={classes.details}>
                                            <CardContent className={classes.content}>
                                                <Grid container direction="row" justify="flex-start" alignItems="flex-start" spacing={1}>
                                                    <Grid item xs={5}>
                                                        <Grid item xs={12}>
                                                            <Typography component="h5" variant="h5">
                                                                {product.Title}
                                                            </Typography>
                                                        </Grid>
                                                        <Grid item xs={12} component="ul" className={classes.chip}>
                                                            {this.renderCategorys(product.Category)}
                                                        </Grid>
                                                    </Grid>
                                                    <Grid item xs container alignItems="center">
                                                        <Button variant="contained" color="primary" onClick={() => { this.props.deleteProductCard(product) }}>
                                                            <DeleteIcon /> 
                                                        </Button>
                                                    </Grid>
                                                    <Grid item xs>
                                                        <Grid item xs={12}>
                                                            <Typography variant="body1" gutterBottom>
                                                                Unit Price
                                                            </Typography>
                                                        </Grid>
                                                        <Grid item xs={12}>
                                                            <Typography component="h6" variant="h6">
                                                                $ {product.Price}
                                                            </Typography>
                                                        </Grid>
                                                    </Grid>
                                                    <Grid item xs={2}>
                                                        <Grid item xs={12}>
                                                            <FormControl className={classes.formControl}>
                                                                <InputLabel id={`sort${product.Id}`}>Qty</InputLabel>
                                                                <Select
                                                                    labelId={`sort${product.Id}`}
                                                                    id={`${product.Id}`}
                                                                    name={`${product.Id}`}
                                                                    open={this.state[`open${product.Id}`]}
                                                                    onClose={this.handleClose}
                                                                    onOpen={this.handleOpen}
                                                                    value={this.state[`qty${product.Id}`]}
                                                                    onChange={this.handleChange}
                                                                >
                                                                <MenuItem value={1}>1</MenuItem>
                                                                <MenuItem value={2}>2</MenuItem>
                                                                <MenuItem value={3}>3</MenuItem>
                                                                <MenuItem value={4}>4</MenuItem>
                                                                </Select>
                                                            </FormControl>
                                                        </Grid>
                                                    </Grid>
                                                    <Grid item xs>
                                                        <Typography component="h5" variant="h5">
                                                            $ { parseFloat(product.Price) * parseInt(this.state[`qty${product.Id}`]) }
                                                        </Typography>
                                                    </Grid>
                                                </Grid>
                                            </CardContent>
                                        </div>
                                    </Card>
                                </Grid>
                            );
                        }) }
                    </Grid>
                    :
                    <Fragment>
                        <Grid container direction="row" justify="center" alignItems="center" spacing={1}>
                            <Grid item xs={7}>
                                <Box display="flex" flexDirection="row" m={10}>
                                    <Typography component="h1" variant="h1">
                                        Not Products
                                    </Typography>
                                </Box>
                            </Grid>
                        </Grid>
                    </Fragment>
                }
                <Grid container direction="row" justify="flex-start" alignItems="flex-start" spacing={1}>
                    <Grid item xs={10}>
                        <Box display="flex" flexDirection="row" mb={1}>
                            <Link to="/" className={classes.colorLink}>
                                <Typography component="h6" variant="h6">
                                    Continue Shopping
                                </Typography>
                            </Link>
                        </Box>
                    </Grid>
                    <Grid item xs={2}>
                        <Box display="flex" flexDirection="row" mb={1}>
                            <Typography omponent="h5" variant="h5">
                                Total: $ {this.sumTotal()}
                            </Typography>
                        </Box>
                    </Grid>
                </Grid>
                <Grid container direction="row" justify="flex-end" alignItems="flex-end" spacing={1}>
                    <Grid item xs={2}>
                        { this.sumTotal() > 0 ?
                            <Button variant="contained" color="primary" onClick={() => { this.checkout() }}>
                                Check Out
                            </Button>
                            :
                            null
                        }
                    </Grid>
                </Grid>
            </Fragment>
        );
    }
};

const mapStateToProps = (state) => {
    return { 
        cart: state.cart
    };
}

export default connect(mapStateToProps, {deleteProductCard, addQtys})(withStyles(useStyles, { withTheme: true })(ShoppingCart));