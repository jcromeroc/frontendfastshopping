import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { AppBar, Toolbar, Typography, IconButton, Badge } from '@material-ui/core';
import StoreIcon from '@material-ui/icons/Store';
import LocalGroceryStoreIcon from '@material-ui/icons/LocalGroceryStore';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

const useStyles = (theme) => ({
    root: {
      flexGrow: 1,
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    title: {
      flexGrow: 1,
    },
    offset: theme.mixins.toolbar,
    colorLink: {
        color: "white"
    }
});
  
class Header extends Component {
    render() {
        const { classes } = this.props;
        return (
            <div className={classes.root}>
                <AppBar position="fixed" color="primary">
                    <Toolbar>
                        <StoreIcon className={classes.menuButton} />
                        <Typography variant="h6" className={classes.title}>
                            Fast Shopping
                        </Typography>
                        <Link to="/cart" className={classes.colorLink}>
                            <IconButton aria-label="cart badge" color="inherit">
                                <Badge badgeContent={this.props.cart.length} color="secondary">
                                    <LocalGroceryStoreIcon /> 
                                </Badge>
                            </IconButton>
                        </Link>
                    </Toolbar>
                </AppBar>
                <div className={classes.offset}></div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return { 
        cart: state.cart
    };
}

export default connect(mapStateToProps)(withStyles(useStyles, { withTheme: true })(Header));
