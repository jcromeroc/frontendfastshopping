import API from '../api/api';

export const fetchProducts = () => {
    return async dispatch => {
        const response = await API.get(`products/1`);
        dispatch({
            type: 'FETCH_PRODUCTS',
            payload: response.data
        });
    }
};

export const fetchProductsByPageAndOrder = (page, sort) => {
    return async dispatch => {
        const response = await API.get(`products/${page}/${sort}`);
        dispatch({
            type: 'FETCH_PRODUCTS_PAGE_SORT',
            payload: response.data
        });
    }
};

export const addToCart = (product) => {
    return {
        type: 'ADD_TO_CART',
        payload: product
    };
};

export const deleteProductCard = (product) => {
    return {
        type: 'DELETE_PRODUCT_CART',
        payload: product
    };
};

export const addQtys = (qtys) => {
    return {
        type: 'ADD_QTY',
        payload: qtys
    };
};

export const savePage = (page) => {
    return {
        type: 'SAVE_PAGE',
        payload: page
    };
};

export const saveSort = (sort) => {
    return {
        type: 'SAVE_SORT',
        payload: sort
    };
};

export const getUserByEmail = (email) => {
    return async dispatch => {
        const response = await API.get(`customers/${email}`);
        dispatch({
            type: 'GET_USER_BY_EMAIL',
            payload: response.data
        });
    }
};

export const postCustomer = (user) => {
    return async dispatch => {
        const response = await API.post(`customers`, user);
        dispatch({
            type: 'POST_CUSTOMER',
            payload: response.data
        });
    }
};

export const postOrder = (order) => {
    return async dispatch => {
        const response = await API.post(`orders`, order);
        dispatch({
            type: 'POST_ORDER',
            payload: response.data
        });
    }
};

export const postOrderSummary = (summary) => {
    return async dispatch => {
        const response = await API.post(`orders-summary`, summary);
        dispatch({
            type: 'POST_SUMMARY',
            payload: response.data
        });
    }
};

export const saveSummary = (summary) => {
    return {
        type: 'SAVE_SUMMARY',
        payload: summary
    };
};

export const clearAll = () => {
    return {
        type: 'CLEAR_ALL',
    };
};

