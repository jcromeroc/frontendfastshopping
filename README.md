Fast Shopping
===============



Instalación del Frontend
===============

1. Bajar el proyecto y en directorio raíz ejecutar el comando: npm install.
2. Cambiar en el archivo src/api/api.js la línea 4 por la configuración de la ruta que se haya definido al instalar el backend de la aplicación (En el caso de haber cambiado el puerto por defecto).
3. Recuerde que también debe estar corriendo el backend y los servicios que se necesiten: Por ejemplo apache y mysql.



Uso del Frontend
===============

Ejecutar en la raíz del proyecto el comando: npm start.

Al hacer esto el proyecto estará montado en la ruta: http://localhost:3000.

A disfrutar!!!
